(function() {
  var Master, SocketIoInit, master,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  SocketIoInit = require('./lib/SocketIoInit');

  Master = (function(superClass) {
    var words;

    extend(Master, superClass);

    words = ['colombia', 'oscars', 'obama', 'facebook'];

    function Master() {
      Master.__super__.constructor.call(this, 'server');
      this.setEvents();
      this.processors = [];
    }

    Master.prototype.setEvents = function() {
      return this.on('disconnect', (function(_this) {
        return function(socket) {
          if (_this.processors.indexOf(socket) !== -1) {
            return _this.processors.splice(_this.processors.indexOf(socket), 1);
          }
        };
      })(this));
    };

    Master.prototype.initProcess = function() {
      var i, index, len, socket, socketsLenght, word;
      console.log("Starting with " + this.processors.length + " sockets");
      this.io.to('processors').emit('sayHi', {
        server: process.pid
      });
      socketsLenght = this.processors.length;
      for (index = i = 0, len = words.length; i < len; index = ++i) {
        word = words[index];
        socket = this.processors[index % socketsLenght];
        socket.emit('listenWord', word);
        socket.assignedWords.push(word);
      }
      return this.interval = setInterval((function(_this) {
        return function() {
          return _this.io.to('processors').emit('getStatistics');
        };
      })(this), 1000);
    };

    Master.prototype.stopProcess = function() {
      console.log('Process finish successfully');
      clearInterval(this.interval);
      return this.io.to('processors').emit('stopListening');
    };

    Master.prototype.socketInit = function(socket) {
      Master.__super__.socketInit.call(this, socket);
      return socket.assignedWords = [];
    };

    Master.prototype.listeners = function(socket) {
      return {
        imBrowser: (function(_this) {
          return function() {
            console.log('Setting up a browser');
            socket.join('browsers');
            return _this.setListeners(socket, {
              initProcess: function() {
                return _this.initProcess();
              },
              stopProcess: function() {
                return _this.stopProcess();
              }
            });
          };
        })(this),
        imProcessor: (function(_this) {
          return function() {
            console.log('Setting up a processor');
            socket.join('processors');
            _this.processors.push(socket);
            return _this.setListeners(socket, {
              receiveStatistics: function(data) {
                console.log((new Date) + "(" + data.word + ") partial:" + data.statistics.totalSinceLastGet);
                return _this.io.to('browsers').emit('setData', data);
              }
            });
          };
        })(this)
      };
    };

    return Master;

  })(SocketIoInit);

  master = new Master();

}).call(this);
