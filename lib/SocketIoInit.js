(function() {
  var EventEmitter, SocketIoInit, _,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  _ = require('underscore');

  EventEmitter = require('events').EventEmitter;

  SocketIoInit = (function(superClass) {
    extend(SocketIoInit, superClass);

    SocketIoInit.prototype.defaults = {
      host: 'localhost',
      port: '8181'
    };

    function SocketIoInit(role, options) {
      this.role = role != null ? role : 'client';
      if (options == null) {
        options = {};
      }
      this.options = _.extend(_.clone(this.defaults), options);
      this[this.role](options);
    }

    SocketIoInit.prototype.server = function() {
      this.sockets = [];
      this.io = require('socket.io')(this.options.port);
      return this.io.sockets.on('connection', (function(_this) {
        return function(socket) {
          _this.sockets.push(socket);
          _this.socketInit(socket);
          return _this.setListeners(socket);
        };
      })(this));
    };

    SocketIoInit.prototype.client = function() {
      this.io = require('socket.io-client');
      this.connection = this.io.connect("http://" + this.options.host + ":" + this.options.port);
      return this.connection.on('connect', (function(_this) {
        return function() {
          _this.setListeners();
          return _this.clientInit();
        };
      })(this));
    };

    SocketIoInit.prototype.clientInit = function() {};

    SocketIoInit.prototype.setListeners = function(socket, listeners) {
      var listener, listenerSetter, results;
      if (socket == null) {
        socket = false;
      }
      if (listeners == null) {
        listeners = false;
      }
      listeners || (listeners = _.extend(_.clone(this._listeners(socket)), this.listeners(socket)));
      listenerSetter = this.role !== 'client' && socket ? socket : this.connection;
      results = [];
      for (listener in listeners) {
        results.push(listenerSetter.on(listener, listeners[listener]));
      }
      return results;
    };

    SocketIoInit.prototype._listeners = function(socket) {
      return {
        sayHi: (function(_this) {
          return function(data) {
            return console.log("I'm " + process.pid + " and have a new connection with " + data.server);
          };
        })(this)
      };
    };

    SocketIoInit.prototype.socketInit = function(socket) {
      return socket.on('disconnect', (function(_this) {
        return function() {
          return _this.socketDisconnect(socket);
        };
      })(this));
    };

    SocketIoInit.prototype.socketDisconnect = function(socket) {
      if (this.role === 'server') {
        if (this.sockets.indexOf(socket) !== -1) {
          this.sockets.splice(this.sockets.indexOf(socket), 1);
        }
        return this.emit('disconnect', socket);
      }
    };

    SocketIoInit.prototype.listeners = function(socket) {
      return {};
    };

    return SocketIoInit;

  })(EventEmitter);

  module.exports = SocketIoInit;

}).call(this);
