# README #

This is the simple readme for the tweet-word-count Socket.IO sample application for servers

### Clon it! ###

* git clone https://bitbucket.org/sposmen/tweet-word-count.git

### Install packages ###

* `cd tweet-word-count && npm install`

### Ask for the server pointer ###

* Duh?

### Run the client ###

* `node client.js -h [DUH Response]`
