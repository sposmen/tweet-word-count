(function() {
  var Client, SocketIoInit, client, fs, minimist, path,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  path = require('path');

  minimist = require('minimist');

  SocketIoInit = require(path.join(__dirname, 'lib', 'SocketIoInit'));

  fs = require('graceful-fs');

  Client = (function(superClass) {
    extend(Client, superClass);

    function Client() {
      var argv, options;
      argv = minimist(process.argv.slice(2));
      options = {};
      if (argv.host || argv.h) {
        options.host = argv.host || argv.h;
      }
      Client.__super__.constructor.call(this, 'client', options);
      this.trackedWords = {};
    }

    Client.prototype.trackWord = function(word) {
      var t;
      t = {};
      return this.trackedWords[word] = t;
    };

    Client.prototype.clientInit = function() {
      return this.connection.emit('imProcessor');
    };

    Client.prototype.listeners = function() {
      return {
        listenWord: (function(_this) {
          return function(word) {
            if (_this.trackedWords[word] == null) {
              return _this.trackWord(word);
            }
          };
        })(this),
        stopListening: (function(_this) {
          return function() {
            return _this.trackedWords = {};
          };
        })(this),
        getStatistics: (function(_this) {
          return function() {
            var results, t, word;
            results = [];
            for (word in _this.trackedWords) {
              t = {
                word: word,
                statistics: {
                  totalSinceLastGet: Math.floor(Math.random() * 50)
                }
              };
              console.log(t);
              results.push(_this.connection.emit('receiveStatistics', t));
            }
            return results;
          };
        })(this)
      };
    };

    return Client;

  })(SocketIoInit);

  client = new Client();

}).call(this);
